import sys
from copy import copy
import numpy as np
import re
from collections import Counter


def read_data(fname):
    file1 = open(fname, "r")
    lines = file1.readlines()
    map = np.zeros((len(lines), len(lines)), dtype=int)
    for i_line in range(len(lines)):
        map[i_line, :] = re.findall(r'\d', lines[i_line])
    map_big = np.zeros((len(lines) * 5, len(lines) * 5), dtype=int)
    for k_row in range(5):
        for k_col in range(5):
            row_start = len(map) * k_row
            row_end = len(map) * (k_row + 1)
            col_start = len(map) * k_col
            col_end = len(map) * (k_col + 1)
            map_part = copy(map)
            map_part += k_row + k_col
            map_part[map_part > 9] -= 9
            map_big[row_start:row_end, col_start:col_end] = map_part
    return map_big


def get_sort_neighbor(map, coord_point):
    steps = [[0, 1], [1, 0], [-1, 0], [0, -1]]
    coords_neighbor = []
    for step in steps:
        coord_neighbor = [coord_point[0] + step[0], coord_point[1] + step[1]]
        if 0 <= coord_neighbor[0] < len(map) and 0 <= coord_neighbor[1] < len(map):
            coords_neighbor.append(coord_neighbor)
    neighbors = []
    for coord_neighbor in coords_neighbor:
        neighbors.append([map[coord_neighbor[0], coord_neighbor[1]], coord_neighbor])
    neighbors.sort(key=lambda x: x[0])
    coords_neighbor = [neighbor[1] for neighbor in neighbors]
    return coords_neighbor


def calc_risk_in_point(map, map_total_risk, coord_point, path):
    total_risk_point = map_total_risk[coord_point[0], coord_point[1]]
    coords_neighbor = get_sort_neighbor(map, coord_point)
    for coord_neighbor in coords_neighbor:
        if coord_neighbor in path:
            continue
        total_risk_neighbor_point = map_total_risk[coord_neighbor[0], coord_neighbor[1]]
        new_total_risk_neighbor_point = total_risk_point + map[coord_neighbor[0], coord_neighbor[1]]
        if new_total_risk_neighbor_point < total_risk_neighbor_point:
            path.append(coord_neighbor)
            map_total_risk[coord_neighbor[0], coord_neighbor[1]] = new_total_risk_neighbor_point
            calc_risk_in_point(map, map_total_risk, coord_neighbor, copy(path))
    test = 1


if __name__ == "__main__":
    sys.setrecursionlimit(250000)
    map = read_data('input.txt')
    map_total_risk = np.ones((len(map), len(map)), dtype=int) * 99999
    map_total_risk[0, 0] = 0
    path = []
    calc_risk_in_point(map, map_total_risk, [0, 0], path)
    test = 1
