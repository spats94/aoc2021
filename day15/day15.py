import sys
from copy import copy
import numpy as np
import re
import networkx as nx
from collections import Counter


def read_data(fname):
    file1 = open(fname, "r")
    lines = file1.readlines()
    map = np.zeros((len(lines), len(lines)), dtype=int)
    for i_line in range(len(lines)):
        map[i_line, :] = re.findall(r'\d', lines[i_line])
    map_big = np.zeros((len(lines) * 5, len(lines) * 5), dtype=int)
    for k_row in range(5):
        for k_col in range(5):
            row_start = len(map) * k_row
            row_end = len(map) * (k_row + 1)
            col_start = len(map) * k_col
            col_end = len(map) * (k_col + 1)
            map_part = copy(map)
            map_part += k_row + k_col
            map_part[map_part > 9] -= 9
            map_big[row_start:row_end, col_start:col_end] = map_part
    return map, map_big


def get_graph(map):
    graph_point = nx.DiGraph()
    steps = [[0, 1], [1, 0], [-1, 0], [0, -1]]
    for row in range(len(map)):
        for col in range(len(map)):
            for step in steps:
                graph_point.add_node(tuple([row, col]))
                coord_neighbor = [row + step[0], col + step[1]]
                if 0 <= coord_neighbor[0] < len(map) and 0 <= coord_neighbor[1] < len(map):
                    graph_point.add_node(tuple(coord_neighbor))
                    graph_point.add_edge(tuple([row, col]), tuple(coord_neighbor), weight=map[coord_neighbor[0],
                                                                                              coord_neighbor[1]])
    return graph_point


def get_total_risk(map):
    graph_point = get_graph(map)
    path = nx.shortest_path(graph_point, source=tuple([0, 0]),
                            target=tuple([len(map) - 1, len(map) - 1]), weight='weight')
    total_risk = 0
    for i in range(1, len(path)):
        total_risk += map[path[i][0], path[i][1]]
    return total_risk


if __name__ == "__main__":
    sys.setrecursionlimit(250000)
    map, map_big = read_data('input.txt')
    print('part 1 =', get_total_risk(map))
    print('part 2 =', get_total_risk(map_big))
