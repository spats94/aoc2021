from copy import copy

import numpy as np
import re


def read_data(fname):
    file1 = open(fname, "r")
    lines = file1.readlines()
    dict_map_caves = {}
    for connection_caves in lines:
        cave, neighbor_cave = re.findall(r'\w+', connection_caves)
        if cave not in dict_map_caves.keys():
            dict_map_caves.update({cave: [neighbor_cave]})
        else:
            dict_map_caves[cave].append(neighbor_cave)
        if neighbor_cave not in dict_map_caves.keys():
            dict_map_caves.update({neighbor_cave: [cave]})
        else:
            dict_map_caves[neighbor_cave].append(cave)
    return dict_map_caves


def do_step_to_cave_part1(map_caves, cave, path, list_paths):
    path.append(cave)
    if cave == 'end':
        list_paths.append(path)
        return
    for cave_neighbor in map_caves[cave]:
        if cave_neighbor.isupper() or cave_neighbor not in path:
            do_step_to_cave_part1(map_caves, cave_neighbor, copy(path), list_paths)


def count_small_cave_twice(path):
    for cave in path:
        if cave.islower() and path.count(cave) == 2:
            return True
    else:
        return False


def do_step_to_cave_part2(map_caves, cave, path, list_paths):
    path.append(cave)
    if cave == 'end':
        list_paths.append(path)
        return
    for cave_neighbor in map_caves[cave]:
        if cave_neighbor == 'start':
            continue
        if cave_neighbor.isupper() or not count_small_cave_twice(path) or path.count(cave_neighbor) == 0:
            do_step_to_cave_part2(map_caves, cave_neighbor, copy(path), list_paths)


if __name__ == "__main__":
    map_caves = read_data('input.txt')
    list_paths = []
    path = []
    do_step_to_cave_part1(map_caves, 'start', path, list_paths)
    list_paths = []
    path = []
    do_step_to_cave_part2(map_caves, 'start', path, list_paths)
    stop = 1
