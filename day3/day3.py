import numpy as np


def read_data(fname):
    file1 = open(fname, "r")
    lines = file1.readlines()
    numbers = np.fromstring(lines[0], dtype=int, sep=',')
    boards = []
    len_board = len(np.fromstring(lines[2], dtype=int, sep=' '))
    board = np.zeros((len_board, len_board), dtype=int)
    boards.append(board)
    board = boards[0]
    j = 0
    for i in range(2, len(lines)):
        if lines[i] == '\n':
            boards.append(np.zeros((len_board, len_board), dtype=int))
            j = 0
            board = boards[-1]
            continue
        line = np.fromstring(lines[i], dtype=int, sep=' ')
        board[j][:] = line
        j += 1
    file1.close()
    return numbers, boards


def check_board_for_win(board, row, col):
    return np.all(board[row, :] == 1) or np.all(board[:, col])


def calc_result(board_draw, board, number, result_list):
    if np.all(board_draw[:, :] == 1):
        return 0
    sum1 = 0
    for row in range(len(board)):
        for col in range(len(board)):
            if board_draw[row, col] == 0:
                sum1 += board[row, col]
    result_list.append(sum1 * number)
    board_draw[:, :] = 1
    return


def play_game(numbers, boards):
    boards_draw = []
    result_list = []
    for i in range(len(boards)):
        boards_draw.append(np.zeros((len(boards[0]), len(boards[0])), dtype=int))
    for number in numbers:
        for i, board in enumerate(boards):
            row, col = np.where(board == number)
            if len(row) == 0:
                continue
            boards_draw[i][row, col] = 1
            if check_board_for_win(boards_draw[i], row, col):
                calc_result(boards_draw[i], board, number, result_list)
    return result_list


if __name__ == "__main__":
    numbers, boards = read_data('input.txt')
    result_list = play_game(numbers, boards)
    print('Ответ на первую часть', result_list[0])
    print('Ответ на вторую часть', result_list[-1])
