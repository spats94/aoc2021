from copy import copy

import numpy as np
import re


def read_data(fname):
    file1 = open(fname, "r")
    lines = file1.readlines()
    map = np.zeros((10, 10), dtype=int)
    for row, line in enumerate(lines):
        map[row, :] = re.findall(r'\d', line)
    return map

def do_blick(row, col, map):
    map[row, col] = 0
    steps = [[1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1]]
    for step in steps:
        row_neighboor = row + step[0]
        col_neighboor = col + step[1]
        test = len(map)
        if not (0 <= row_neighboor < len(map) and 0 <= col_neighboor < len(map)):
            continue
        if map[row_neighboor, col_neighboor] == 10:
            do_blick(row_neighboor, col_neighboor, map)
        if 0 < map[row_neighboor, col_neighboor] < 10:
            map[row_neighboor, col_neighboor] += 1


if __name__ == "__main__":
    map = read_data('input.txt')
    map[:, :] += 1
    result_part1 = 0
    for step in range(100):
        for row in range(10):
            for col in range(10):
                if 1 <= map[row, col] < 10:
                    map[row, col] += 1
                elif map[row, col] == 10:
                    do_blick(row, col, map)
        for row in range(10):
            for col in range(10):
                if map[row, col] == 0:
                    result_part1 += 1
                    map[row, col] = 1
        print('After step', step + 1)
        print(map[:, :] - 1)

    #part 2
    map = read_data('input.txt')
    map[:, :] += 1
    result_part1 = 0
    step = 1
    while True:
        for row in range(10):
            for col in range(10):
                if 0 < map[row, col] < 10:
                    map[row, col] += 1
                elif map[row, col] == 10:
                    do_blick(row, col, map)
        for row in range(10):
            for col in range(10):
                if map[row, col] == 0:
                    map[row, col] = 1
        print('After step', step)
        step += 1
        print(map[:, :] - 1)
        if np.all(map == 1):
            break
    list_result_number = []
