from copy import copy
import numpy as np
import re
from collections import Counter

def read_data(fname):
    file1 = open(fname, "r")
    lines = file1.readlines()
    polymer_template = re.findall(r'\w', lines[0])
    dict_insertion_rules = {}
    for i_line in range(2, len(lines)):
        insertion_rules = re.findall(r'\w+', lines[i_line])
        dict_insertion_rules.update({insertion_rules[0]: insertion_rules[1]})
        test = 1
    return polymer_template, dict_insertion_rules


if __name__ == "__main__":
    polymer_template, dict_insertion_rules = read_data('input_test.txt')
    new_polymer = [polymer_template[0]]
    polymer = copy(polymer_template)
    for step in range(1, 41):
        for i in range(len(polymer) - 1):
            pair_elem = str(polymer[i]) + str(polymer[i + 1])
            if pair_elem in dict_insertion_rules.keys():
                new_polymer.append(dict_insertion_rules[pair_elem])
                new_polymer.append(polymer[i + 1])
            else:
                new_polymer.append(polymer[i + 1])
        polymer = copy(new_polymer)
        new_polymer = [polymer[0]]
        print('After step', step, ':',  ''.join(polymer))
    c = Counter(polymer)
    print(c['C'] - c['H'])
    test = 1
