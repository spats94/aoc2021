from copy import copy

import numpy as np
import re


def read_data(fname):
    file1 = open(fname, "r")
    lines = file1.readlines()
    output = []
    for line in lines:
        parse_line = re.findall(r'[a-z]+|[|]', line)
        patterns = parse_line[:parse_line.index('|')]
        output_digit = parse_line[parse_line.index('|') + 1:]
        output.append({'patterns': patterns, 'output_digit': output_digit})
    return output


def check_pattern_len_number(dict_pattern_possible_numb):
    global dict_digits_pattern_default
    for pattern in dict_pattern_possible_numb:
        for digit in dict_digits_pattern_default:
            test = len(dict_digits_pattern_default[digit])
            if len(pattern) != len(dict_digits_pattern_default[digit]):
                dict_pattern_possible_numb[pattern].remove(digit)
    test = 1


def check_pattern(dict_incorrect_patterns, table_possible_pattern, dict_digits_pattern_default):
    for pattern in dict_incorrect_patterns:
        if len(dict_incorrect_patterns[pattern]) == 1:
            numb = dict_incorrect_patterns[pattern][0]
            for razryad in table_possible_pattern:
                if razryad in dict_digits_pattern_default[numb]:
                    table_possible_pattern[razryad] = set(table_possible_pattern[razryad]) & set(
                        razryad for razryad in pattern)
                    test1 = table_possible_pattern[razryad]
                else:
                    table_possible_pattern[razryad] = set(table_possible_pattern[razryad])
                    table_possible_pattern[razryad] -= set(razryad for razryad in pattern)
                    test1 = 1


def find_three(dict_incorrect_patterns):
    for incorrect_pattern in dict_incorrect_patterns:
        if dict_incorrect_patterns[incorrect_pattern] == [1]:
            incorrect_pattern_one = incorrect_pattern
            break
    for incorrect_pattern in dict_incorrect_patterns:
        if dict_incorrect_patterns[incorrect_pattern] == [2, 3, 5]:
            if set(char for char in incorrect_pattern_one) <= set(char for char in incorrect_pattern):
                dict_incorrect_patterns[incorrect_pattern] = [3]
    test = 1


def find_six(dict_incorrect_patterns):
    for incorrect_pattern in dict_incorrect_patterns:
        if dict_incorrect_patterns[incorrect_pattern] == [1]:
            incorrect_pattern_one = incorrect_pattern
            break
    for incorrect_pattern in dict_incorrect_patterns:
        if dict_incorrect_patterns[incorrect_pattern] == [0, 6, 9]:
            if not set(char for char in incorrect_pattern_one) <= set(char for char in incorrect_pattern):
                dict_incorrect_patterns[incorrect_pattern] = [6]
    test = 1



if __name__ == "__main__":
    list_output = read_data('input.txt')
    list_result_numder = []
    for output in list_output:
        dict_digits_pattern_default = {
            0: ('a', 'b', 'c', 'e', 'f', 'g'),
            1: ('c', 'f'),
            2: ('a', 'c', 'd', 'e', 'g'),
            3: ('a', 'c', 'd', 'f', 'g'),
            4: ('b', 'c', 'd', 'f'),
            5: ('a', 'b', 'd', 'f', 'g'),
            6: ('a', 'b', 'd', 'e', 'f', 'g'),
            7: ('a', 'c', 'f'),
            8: ('a', 'b', 'c', 'd', 'e', 'f', 'g'),
            9: ('a', 'b', 'c', 'd', 'f', 'g')
        }
        table_possible_pattern = {'a': ('a', 'b', 'c', 'd', 'e', 'f', 'g'),
                                  'b': ('a', 'b', 'c', 'd', 'e', 'f', 'g'),
                                  'c': ('a', 'b', 'c', 'd', 'e', 'f', 'g'),
                                  'd': ('a', 'b', 'c', 'd', 'e', 'f', 'g'),
                                  'e': ('a', 'b', 'c', 'd', 'e', 'f', 'g'),
                                  'f': ('a', 'b', 'c', 'd', 'e', 'f', 'g'),
                                  'g': ('a', 'b', 'c', 'd', 'e', 'f', 'g')}
        incorrect_patterns = output['patterns']
        dict_incorrect_patterns = {pattern: [possible_number for possible_number in range(10)] for pattern in incorrect_patterns}
        check_pattern_len_number(dict_incorrect_patterns)
        find_three(dict_incorrect_patterns)
        find_six(dict_incorrect_patterns)
        check_pattern(dict_incorrect_patterns, table_possible_pattern, dict_digits_pattern_default)
        correct_patterns = []
        incorrect_displays = copy(output['output_digit'])
        correct_displays = []
        for incorrect_pattern in incorrect_displays:
            str1 = ''
            for char_incorrect_pattern in incorrect_pattern:
                for char_correct in table_possible_pattern:
                    if re.findall(r'[a-z]', str(table_possible_pattern[char_correct]))[0] == char_incorrect_pattern:
                        break

                str1 += re.findall(r'[a-z]', str(char_correct))[0]
            correct_displays.append(str1)

        result_numder = ''
        for correct_display in correct_displays:
            for digits_pattern in dict_digits_pattern_default:
                test1 = set(char for char in correct_display)
                test2 = set(dict_digits_pattern_default[digits_pattern])
                test3 = test2 ^ test1
                test = 1
                if set(char for char in correct_display) >= set(dict_digits_pattern_default[digits_pattern]) >= set(char for char in correct_display):
                    result_numder += str(digits_pattern)
        list_result_numder.append(int(result_numder))
    print(list_result_numder)
    sum = 0
    for result_numder in list_result_numder:
        sum += result_numder
    print(sum)

