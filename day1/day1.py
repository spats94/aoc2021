import numpy as np


def read_data(fname):
    dtype_data = [('depths', np.int32)]
    data = np.fromregex(fname, r'(\d+)', dtype_data)['depths']
    return data

if __name__== "__main__":
    input_data = read_data('input_test.txt')
    test = 1