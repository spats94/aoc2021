import binascii
import sys
from copy import copy
import numpy as np
import re
import networkx as nx
from collections import Counter


def read_data(fname):
    file1 = open(fname, "r")
    lines = file1.readlines()[0]
    int_value = bin(int(lines, base=16))[2::]
    h_size = len(lines) * 4
    int_value = (int_value).zfill(h_size)
    return int_value


def read_literal_value(binary_string):
    V = int(binary_string[0:3], base=2)
    binary_string = binary_string[3::]
    T = int(binary_string[0:3], base=2)
    binary_string = binary_string[3::]
    number_bin = ''
    while True:
        number_bin += binary_string[1:5]
        if binary_string[0] == '1':
            binary_string = binary_string[5::]
        else:
            binary_string = binary_string[5::]
            break
    return V, T, number_bin, binary_string


def parse_bin(binary_string, V_sum):
    T = int(binary_string[3:6], base=2)
    if T == 4:
        V, T, number_bin, binary_string = read_literal_value(binary_string)
        V_sum[0] += V
        return binary_string
    else:
        V = int(binary_string[0:3], base=2)
        V_sum[0] += V
        binary_string = binary_string[6::]
        I = int(binary_string[0], base=2)
        binary_string = binary_string[1::]
        if I == 0:
            L = int(binary_string[0:15], base=2)
            binary_string = binary_string[15::]
            binary_string_part = binary_string[0:L]
            binary_string = binary_string[L::]
            while True:
                binary_string_part = parse_bin(binary_string_part, V_sum)
                if not binary_string_part:
                    return binary_string
                if len(binary_string_part) == 0:
                    return binary_string
        else:
            L = int(binary_string[0:11], base=2)
            binary_string = binary_string[11::]
            for i in range(L):
                binary_string = parse_bin(binary_string, V_sum)
    return binary_string


if __name__ == "__main__":
    binary_string = read_data('input.txt')
    V_sum = [0]
    while True:
        binary_string = parse_bin(binary_string, V_sum)
        test = 1
