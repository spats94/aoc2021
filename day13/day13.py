from copy import copy

import numpy as np
import re


def read_data(fname):
    file1 = open(fname, "r")
    lines = file1.readlines()
    list_dots = []
    list_instruction = []
    for line in lines:
        if re.match(r'\d+,\d+', line):
            x, y = re.findall(r'\d+', line)
            list_dots.append([x, y])
        if line == '\n':
            continue
        if re.match(r'fold along \w+=\d+', line):
            test = re.findall(r'[x|y]|\d+', line)
            direction, value = re.findall(r'[x|y]|\d+', line)
            list_instruction.append([direction, value])
            list_instruction[-1][1] = list_instruction[-1][1]
    for i in range(len(list_dots)):
        list_dots[i][0] = int(list_dots[i][0])
        list_dots[i][1] = int(list_dots[i][1])
    for i in range(len(list_instruction)):
        list_instruction[i][1] = int(list_instruction[i][1])
    return list_dots, list_instruction


def create_map(list_dots):
    x_max = 0
    y_max = 0
    for dots in list_dots:
        if x_max < dots[0]:
            x_max = dots[0]
        if y_max < dots[1]:
            y_max = dots[1]
    map_dots = np.zeros((y_max+1, x_max+1), dtype=int)
    for dot in list_dots:
        map_dots[dot[1], dot[0]] = 1
    return map_dots


def do_fold_y(map_dots, instruction):
    len_up_part_y = instruction[1]
    len_down_part_y = len(map_dots[:, 0]) - instruction[1] - 1
    if len_up_part_y >= len_down_part_y:
        new_up_map_dots = map_dots[:len_up_part_y, :]
        new_down_map_dots = map_dots[len(map_dots[:, 0]) - instruction[1]:, :]
        new_down_map_dots = new_down_map_dots[::-1, :]
        y_up = len(new_up_map_dots[:, 0]) - 1
        y_down = len(new_down_map_dots[:, 0]) - 1
        new_maps_dots = new_up_map_dots
        while y_up >= 0 and y_down >= 0:
            test = 1
            new_maps_dots[y_up, :] += new_down_map_dots[y_down, :]
            y_up -= 1
            y_down -= 1
        new_maps_dots[new_maps_dots > 1] = 1
        test = 1
    else:
        new_up_map_dots = map_dots[:len_up_part_y, :]
        new_up_map_dots = new_up_map_dots[::-1, :]
        new_down_map_dots = map_dots[len(map_dots[:, 0]) - instruction[1]:, :]
        y_up = 0
        y_down = 0
        new_maps_dots = new_down_map_dots
        while y_up < len(new_up_map_dots[:, 0]) and y_down < len(new_down_map_dots[:, 0]):
            test = 1
            new_maps_dots[y_down, :] += new_up_map_dots[y_up, :]
            y_up += 1
            y_down += 1
        new_maps_dots[new_maps_dots > 1] = 1
    return new_maps_dots


def do_fold_x(map_dots, instruction):
    len_left_part_y = instruction[1]
    len_right_part_y = len(map_dots[0, :]) - instruction[1] - 1
    if len_left_part_y >= len_right_part_y:
        new_left_part_y = map_dots[:, :len_left_part_y]
        new_right_map_dots = map_dots[:, len(map_dots[0, :]) - instruction[1]:]
        new_right_map_dots = new_right_map_dots[:, ::-1]
        x_left = len(new_left_part_y[0, :]) - 1
        x_right = len(new_right_map_dots[0, :]) - 1
        new_maps_dots = new_left_part_y
        while x_left >= 0 and x_right >= 0:
            new_maps_dots[:, x_left] += new_right_map_dots[:, x_right]
            x_left -= 1
            x_right -= 1
        new_maps_dots[new_maps_dots > 1] = 1
        test = 1
    else:
        new_left_part_y = map_dots[:, :len_left_part_y]
        new_left_part_y = new_left_part_y[:, ::-1]
        new_right_map_dots = map_dots[:, len(map_dots[0, :]) - instruction[1]:]
        x_left = 0
        x_right = 0
        new_maps_dots = new_right_map_dots
        while x_left < len(new_left_part_y[0, :]) and x_right < len(new_right_map_dots[0, :]):
            new_maps_dots[:, x_right] += new_left_part_y[:, x_left]
            x_left += 1
            x_right += 1
        new_maps_dots[new_maps_dots > 1] = 1
        test = 1
    return new_maps_dots


if __name__ == "__main__":
    list_dots, list_instruction = read_data('input.txt')
    map_dots = create_map(list_dots)
    for instruction in list_instruction:
        if instruction[0] == 'x':
            map_dots = do_fold_x(map_dots, instruction)
        if instruction[0] == 'y':
            map_dots = do_fold_y(map_dots, instruction)
    stop1 = 1

